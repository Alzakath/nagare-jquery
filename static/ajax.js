(function ($) {
    "use strict";

    var NAGARE_CONTENT_TYPE = 'application/json',
        asyncAction = function (event) {

            var $target = $(event.currentTarget),
                action = $target.data('nagare-action'),
                value = $target.attr('type') === 'checkbox' && !$target.prop('checked') ? "" : $target.val();

            switch (action.priority) {
                case 4:
                    $target.trigger('postAndEval.nagare');
                    break;
                case 41:
                case 2:
                    $target.trigger('getAndEval.nagare');
                    break;
                case 5:
                    $target.trigger('imageInputSubmit.nagare', [event.clientX, event.clientY]);
                    break;
                case 1:
                    $target.trigger('getAndEval.nagare', [encodeURIComponent(value)]);
                    break;
                default:
                    console.log('Unknown priority: ', action.priority);
            }

            if ($.inArray(action.priority, [4, 41, 5]) !== -1) {
                event.preventDefault();
                event.stopPropagation();
            }

        },
        registerCallbacks = function ($node) {

            if ($node) {
                $("[data-nagare-action]", $node).bind('click.nagare change.nagare', asyncAction);
            } else {
                $("[data-nagare-action]").bind('click.nagare change.nagare', asyncAction);
            }

        },
        unRegisterCallbacks = function ($node) {
            if ($node) {
                $("[data-nagare-action]", $node).unbind('click.nagare change.nagare', asyncAction);
            } else {
                $("[data-nagare-action]").unbind('click.nagare change.nagare', asyncAction);
            }
        },
        Done = function (data) {
            var action = data[0],
                nodeId = data[1],
                $html = $.parseHTML(data[2]),
                $node = $('#' + nodeId);

            unRegisterCallbacks($node);

            if (action === 'nagare_replaceNode') {
                $node.replaceWith($html);
                $node = $html;
            }

            if (action === 'nagare_updateNode') {
                $node.html($html);
            }

            $(function () {
                registerCallbacks();
            });

        },
        Fail = function () {
            unRegisterCallbacks(this);
        },
        getAndEval = function (event, value) {
            var $target = $(event.target),
                url = '?' + $target.data('nagare-action').session + ';_a;' + $target.data('nagare-action').action;

            if (value !== undefined) {
                url += '=' + value
            }

            console.log(url);

            $.ajax({
                beforeSend: function (req) {
                    req.setRequestHeader('Accept', NAGARE_CONTENT_TYPE);
                },
                url: url,
                dataType: 'json',
                context: $target.closest('.nagare-async-view'),
                headers: {}
            }).done(Done).fail(Fail);

        },
        postAndEval = function (event) {

            var $target = $(event.target),
                form = new FormData(event.target.form),
                action = $target.data('nagare-action').action;

            $.ajax({
                beforeSend: function (req) {
                    req.setRequestHeader('Accept', NAGARE_CONTENT_TYPE);
                },
                type: "POST",
                url: '?_a;' + action,
                data: form,
                processData: false,
                contentType: false,
                dataType: 'json',
                context: $target.closest('.nagare-async-view'),
                headers: {}
            }).done(Done).fail(Fail);

        },
        imageInputSubmit = function (event, clientX, clientY) {
            console.log('imageInputSubmit', event);

            var $target = $(event.target),
                form = new FormData(event.target.form),
                action = $target.data('nagare-action').action,
                offset = $target.offset();

            action = action + ".x=" + Math.round(clientX - offset.left) + ';' + action + ".y=" + Math.round(clientY - offset.top);

            $.ajax({
                beforeSend: function (req) {
                    req.setRequestHeader('Accept', NAGARE_CONTENT_TYPE);
                },
                type: "POST",
                url: '?_a;' + action,
                data: form,
                processData: false,
                contentType: false,
                dataType: 'json',
                context: $target.closest('.nagare-async-view'),
                headers: {}
            }).done(Done).fail(Fail);

        };


    // find nagare actions
    $(function () {
        registerCallbacks();
        $(document).on('imageInputSubmit.nagare', imageInputSubmit);
        $(document).on('postAndEval.nagare', postAndEval);
        $(document).on('getAndEval.nagare', getAndEval);
    });

}(jQuery));